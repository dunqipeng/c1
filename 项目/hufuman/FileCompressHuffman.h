//
// Created by 顿 on 2022/10/18 0018.
//
#include <iostream>
#include <string>
#include <vector>
#include "./HuffmanTreeNode.hpp"
#include "Common.h"
using namespace std;
struct  ByteInfo{
    uch _ch;
    size_t _appearCount;
    string _chCode;
    ByteInfo(size_t appearCount=0):_appearCount(appearCount){}
    ByteInfo operator+(const ByteInfo& other)const{
        return ByteInfo(this->_appearCount+other._appearCount);
    }
    bool operator>(const ByteInfo& other)const{
        return _appearCount>other._appearCount;
    }
    bool operator!=(const ByteInfo& other)const{
        return _appearCount!=other._appearCount;
    }
    bool operator==(const ByteInfo& other)const{
        return _appearCount==other._appearCount;
    }
};
class FileCompressHuffman{
public:
    FileCompressHuffman();
    void CompressFile(const string& filePath);
    void UNCompressFile(const string& filePath);
private:
    void GenerateHuffmanCode(HuffmanTreeNode<ByteInfo>* root);
    void writeHeadInfo(const string& filePath,FILE* fOut);
    string GetFilePostFix (const string& filePath);
    void GetLine (FILE*fIn, string& strInfo);
    vector<ByteInfo> _fileInfo;
};
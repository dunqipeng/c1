#include <iostream>
using namespace std;

#include <list>
#include <vector>

//构造函数
void TestList1()
{
	list<int> L1;
	list<int> L2(10, 5);
	for (auto e : L2)
		cout << e << " ";
	cout << endl;

	list<int> L3(L2);
	// list不能通过for+下标方式遍历
	auto it = L3.begin();
	while (it != L3.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	vector<int> v{ 1, 2, 3, 4, 5 };
	list<int> L4(v.begin(), v.end());
	auto rit = L4.rbegin();
	while (rit != L4.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;


	// C++11
	list<int> L5{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	cout << L5.size() << endl;

	L1 = L5;
}


void TestList2()
{
	list<int> L{ 1, 2, 3, 4, 5};
	for (auto e : L)
		cout << e << " ";
	cout << endl;
	cout << L.front() << endl;
	cout << L.back() << endl;

	L.resize(10, 6);
	for (auto e : L)
		cout << e << " ";
	cout << endl;

	L.resize(3);
	for (auto e : L)
		cout << e << " ";
	cout << endl;

	L.clear();
	if (L.empty())
	{
		cout << "list is empty()" << endl;
	}
	else
	{
		cout << "list is not empty()" << endl;
	}
}


template<class T>
void PrintList(const list<T>& L)
{
	for (auto e : L)
		cout << e << " ";
	cout << endl;
}
//insert和erase
void TestList3()
{
	list<int> L;
	L.push_back(1);
	L.push_back(2);
	L.push_back(3);
	L.push_back(4);
	L.push_back(5);
	L.push_back(6);
	L.push_back(7);
	PrintList(L);

	L.pop_back();
	L.pop_back();
	L.pop_back();
	PrintList(L);

	L.push_front(0);
	PrintList(L);

	L.pop_front();
	PrintList(L);

	L.insert(L.begin(), 100);
	L.insert(find(L.begin(), L.end(), 3), 5, 8);
	PrintList(L);

	vector<int> v{ 10, 20, 30 };
	L.insert(L.end(), v.begin(), v.end());
	PrintList(L);

	L.erase(find(L.begin(), L.end(), 3));
	PrintList(L);

	L.erase(find(L.begin(), L.end(), 10), L.end());
	PrintList(L);;

	L.clear();  // L.erase(L.begin(), L.end());
}

///特殊函数
void TestList4()
{
	list<int> L{4,5,3,2,1,5,4,3,1,2,2,2,3,2,3,2,2,2};
	L.remove(2);   // 删除所有值为value的元素
	PrintList(L);

	L.unique();
	PrintList(L);

	L.sort();
	L.unique();   // 去重---注意：去重之前必须报保证list中的元素有序
	PrintList(L);

	L.reverse();
	PrintList(L);
}


// 所有的偶数
bool isOdd(int value)
{
	return !(value & 0x01);
}

// 删除所有5的倍数
bool is(int value)
{
	return 0 == value % 5;
}


void TestList5()
{
	list<int> L{ 4, 5, 3, 2, 1, 5, 4, 3, 1, 2, 2, 2, 3, 2, 3, 2, 2, 2 };
	L.remove_if(isOdd);
	PrintList(L);

	L.remove_if(is);
	PrintList(L);
}

int main()
{
	// TestList1();
	// TestList2();
	// TestList3();
	// TestList4();
	TestList5();
	return 0;
}
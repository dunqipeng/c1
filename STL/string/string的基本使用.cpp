#include<iostream>
using namespace std;
#include <string>
#include <algorithm>
//window环境
void test(){
    string s1("hello");;
    string s2(s1);
    string  s3(10,'a');
    string s4;
    string s5("hello",3);
    char*p="hello";
    string s6(p,p+5);
    //遍历
    for(std::size_t i=0;i<s6.size();i++){
        cout<<s6[i];
    }
    cout<<endl;
    for(auto c:s6){
        cout<<c;
    }
    cout<<endl;
    string::iterator it=s6.begin();
    //把it当成指针(迭代器迭代)
    while(it!=s6.end()){
        cout<<*it;
        ++it;
    }
    cout<<endl;
    string::reverse_iterator it1=s6.rbegin();
    //把it当成指针(迭代器迭代)
    while(it1!=s6.rend()){
        cout<<*it1;
        ++it1;
    }


    //字符串逆转
    reverse(s6.begin(),s6.end());
    cout<<endl<<s6;
    return;
}
//clear
void test1(){
    string s("hello");
    cout<<s.size()<<' '<<s.length()<<' '<<s.capacity()<<endl;
    s.clear();
    cout<<s.size()<<' '<<s.length()<<' '<<s.capacity()<<endl;
}
//resize,总容量不变，只是有效元素改变
/**
将string中有效字符的个数调整到newsize个
resize (newsize，char () ) ;
假设:string中原来字符字符个数为oldsize
原来底层容量为capacity
newsize <= oldsize :将有效元素个数减少到newsize个----注意:不会修改容量
newsize > oldsize:将有效元素个数增多到newsize个
newsize <= capacity:多出来的元素newsize-oldsize直接使用ch填充
newsize > capacity: 1.开辟新空间2.拷贝元素3．释放旧空间4．使用新空间
*/
void test2(){
    string s("hello");
    cout<<s<<' '<<s.size()<<' '<<s.length()<<' '<<s.capacity()<<endl;
    s.resize(20,'a');
    cout<<s<<' '<<s.size()<<' '<<s.length()<<' '<<s.capacity()<<endl;
    s.resize(4);
    cout<<s<<' '<<s.size()<<' '<<s.length()<<' '<<s.capacity()<<endl;
}
//reserve
/*
void reserve (size_t newcapacity):扩容
假设:string底层空间大小为oldcapacity
newcapacity <= oldcapacity:直接忽略oldcapacity>15newcapacity > oldcapacity:才会真正进行扩容---注意:
1.开辟新空间2.拷贝元素3．释放旧空间4.使用新空间
reserve只改变容量，不会修改有效元素个数
*/
void test3(){
    string s("hello");
    cout<<s<<' '<<s.size()<<' '<<s.length()<<' '<<s.capacity()<<endl;
    s.reserve(10);
    s.reserve(20);
    s.reserve(10);
    s.reserve(16);
    cout<<s<<' '<<s.size()<<' '<<s.length()<<' '<<s.capacity()<<endl;
}
void test4(){
    string s("123");
    cout<<s[4];
    cout<<s.at(4);
}
void test5(){
    string s("hello");
    s.push_back(' ');
    s.append("world");

    string s1("!!!");
    s.append(s1);
    s.append(10, '$');

    s += "8888888";
    s += s1;
    s += 'H';
s.assign("895");
    cout<<s;
}
void TestSting10()
{
    string s("helloworld");
    s.insert(s.find('o')+1, 3, ' ');

    // hello   world
    //删除
    s.erase(s.find(' '), 3);
    s.erase(s.begin(), s.begin() + 5);
}
void TestString11()
{
    string s("1234");
    //c_str char*
    int ret = atoi(s.c_str());
    cout<< ret << endl;;
}
// 获取一个文件的后缀
// 20220331.cpp
// F:\abc\xyz.cpp

void TestString12()
{
    string s("20220331.cpp");

    string str = s.substr(s.rfind('.') + 1);
    cout << str << endl;

    string s2("http://www.cplusplus.com/reference/string/string/find/");
    int start = s2.find("://") + 3;
    int end = s2.find('/', start);
    str = s2.substr(start, end - start);
    cout << str << endl;
}
//输入一行最后一个单词大小
void test7(){
    string words;
    while(getline(cin,words)){
        cout<<words.substr(words.rfind(' ')+1).size()<<endl;
    }
}
//可以使用符号比较
void TestString14()
{
    string s1("hello");
    string s2("world");
    if (s1 < s2)
    {
        ;
    }
    else if (s1 > s2)
    {
        ;
    }
    else if (s1 == s2)
    {
        ;
    }
    else
    {
        cout << "肯定不会执行到这里" << endl;
    }
}
int main()
{
    test5();

    return 0;
}

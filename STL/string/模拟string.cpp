#include<iostream>
#include<cstring>
#include<cassert>
using namespace std;
class String{
private:
    char* _str;
    size_t _size;
    size_t _capacity;
    typedef char* iterator;
    typedef char* reverse_iterator;
    static size_t npos;
public:
    String(const char* str=""){
        if(nullptr==str)
            assert(false);
        this->_str=new char [strlen(str)+1];
        strcpy(this->_str,str);
        this->_size=strlen(str);
        this->_capacity=this->_size;
    }
    String(const String& s)
    {
        String temp(s._str);
        this->swap(temp);
    }
    String(int number,char s){
        this->_capacity=number+1;
        this->_size=number;
        this->_str=new char[number+1];
        memset(this->_str,s,number);
        this->_str[number]='\0';
    }
    void swap(String& s){
        std::swap(this->_str,s._str);
        std::swap(this->_size,s._size);
        std::swap(this->_capacity,s._capacity);
    }
    String& operator=(String s){
        this->swap(s);
        return *this;
    }
    ~String(){
        if(this->_str){
            delete [] this->_str;
            this->_str= nullptr;
            this->_size=0;
            this->_capacity=0;
        }
    }
    iterator begin(){
        return this->_str;
    }
    iterator end(){
        return this->_str+this->_size;
    }
    reverse_iterator rbegin(){
        return this->_str+this->_size;
    }
    iterator rend(){
        return this->_str;
    }
    size_t capacity()const{
        return this->_capacity;
    }
    size_t length()const{
        return this->_size;
    }
    size_t empty()const{
        return this->_size==0;
    }
    void reserve(size_t newsize){
        if(newsize>this->capacity()){
            char *temp=new char[newsize+1];
            strcpy(temp,this->_str);
            temp[newsize]='\0';
            delete [] this->_str;
            this->_str=temp;
            this->_capacity=newsize;
        }
    }
    String& append(size_t n,char s){
        if(n+this->_size>this->_capacity)
            reserve(n+this->_size);
        memset(this->_str+this->_size,s,n);
        this->_size+=n;
        this->_str[this->_size]='\0';
        return *this;
    }
    String& append(const char* s){
        if(this->_size+strlen(s)>this->_capacity){
            this->reserve(this->_size+strlen(s));
        }
        strcat(this->_str,s);
        this->_size+=strlen(s);
        return *this;
    }
    void resize(std::size_t newsize,char s){
        if(newsize<=this->_size){
            this->_str[newsize]='\0';
        }else{
            if(newsize>this->capacity())
            this->reserve(newsize);
            this->append(newsize-this->_size,s);
        }
        this->_size=newsize;
    }
    void resize(std::size_t newsize){
        resize(newsize,char());
    }
    void clear(){
        this->_str[0]='\0';
        this->_size=0;
    }
    char& operator[](std::size_t index){
        assert(index<this->_size);
        return this->_str[index];
    }
    const char& operator[](std::size_t index)const{
        assert(index<this->_size);
        return this->_str[index];
    }
    void push_back(char s){
        this->append(1,s);
    }
    String& operator+=(char s){
        this->push_back(s);
        return *this;
    }
    String& operator+=(const char* s){
        this->append(s);
        return *this;
    }
    String& operator+=(String& s){
        this->append(s._str);
        return *this;
    }
    String& insert(std::size_t index,const String& s){
        if(index<0||index>this->_size){
            perror("index error");
            assert(false);
        }
        char*temp=new char[this->_size+s._size+1];
        memcpy(temp,this->_str,index);
        strcpy(temp+index,s._str);
        strcpy(temp+(index+strlen(s._str)),this->_str+index);
        delete [] this->_str;
        this->_str=temp;
        this->_size+=s.length();
        this->_capacity=this->_size;
        return *this;
    }
    String& erase(std::size_t index,std::size_t number){
        if(index<0||index>this->_size||number<0||index+number>this->_size){
            perror("index or number error");
            assert(false);
        }
        char*temp=new char[this->_capacity+1];
        memcpy(temp,this->_str,index);
        strcpy(temp+index,this->_str+index+number);
        delete [] this->_str;
        this->_str=temp;
        this->_size-=number;
        return *this;
    }
    const char* c_str()const{
        return this->_str;
    }
    std::size_t find(char s,std::size_t index=0){
        if(index<0||index>=this->_size){
            perror("index error");
            assert(false);
        }
        for(std::size_t i=0;i<this->_size;i++){
            if(this->_str[i]==s)
                return i;
        }
        return -1;
    }
    String substr(std::size_t index,std::size_t number){
        if(index<0||index>this->_size-number){
            perror("index error");
            assert(false);
        }
        char *temp=new char[number+1];
        strncpy(temp,this->_str+index,number);
        temp[number]='\0';
        String s(temp);
        return s;
    }
    friend ostream& operator<<(ostream& _cout,String& s){
        _cout<<s._str<<' '<<s._size<<' '<<s._capacity<<endl;
        return _cout;
    }
    bool operator<(const String& s){
        return strcmp(this->_str,s.c_str())==-1;
    };
    bool operator<=(const String& s){
        return strcmp(this->_str,s.c_str())==-1 or strcpy(this->_str,s.c_str())==0;
    };
    bool operator>(const String& s){
        return strcmp(this->_str,s.c_str())==1;
    };
    bool operator>=(const String& s){
        return strcmp(this->_str,s.c_str())==1 or strcpy(this->_str,s.c_str())==0;
    };
    bool operator==(const String& s){
       return strcmp(this->_str,s.c_str())==0;
    };
    bool operator!=(const String& s){
        return strcmp(this->_str,s.c_str())!=0;
    };
};
 size_t  String::npos=-1;
int main(){
    String s("0123456789");
    String s1("235");
    cout<< (s<s1);
    return 0;
}

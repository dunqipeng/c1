//传统
class String
{
public:
	String(const char* str = "")
	{
		if (NULL == str)
			str = "";

		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}

	String(const String& s)
		: _str(new char[strlen(s._str)+1])
	{
		strcpy(_str, s._str);
	}

	String& operator=(const String& s)
	{
		if (this != &s)
		{
			char* temp = new char[strlen(s._str) + 1];
			strcpy(temp, s._str);
			delete[] _str;
			_str = temp;

			/*
			delete[] _str;
			_str = new char[strlen(s._str) + 1];
			strcpy(_str, s._str);
			*/
		}

		return *this;
	}

	~String()
	{
		if (_str)
		{
			delete[] _str;
		}
	}
private:
	char* _str;
};

//现版
// 现代版
class String
{
public:
	String(const char* str = "")
	{
		if (NULL == str)
			str = "";

		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}

	String(const String& s)
		: _str(nullptr)
	{
		String strTtemp(s._str);
		swap(_str, strTtemp._str);
	}

	/*
	String& operator=(const String& s)
	{
		if (this != &s)
		{
			String strTtemp(s._str);
			swap(_str, strTtemp._str);
		}

		return *this;
	}
	*/

	String& operator=(String s)
	{
		swap(_str, s._str);
		return *this;
	}

	~String()
	{
		if (_str)
		{
			delete[] _str;
			_str = nullptr;
		}
	}
private:
	char* _str;
};

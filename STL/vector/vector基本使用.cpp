#include<iostream>
using namespace std;
#include<vector>
class Date
{
public:
	Date(int year=1900, int month=1, int day=1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

private:
	int _year;
	int _month;
	int _day;
};


void test1(){
    vector<Date> v0(10);//10个date默认类型的
    //构造方式
    vector<int> v1;
    vector<int> v2(10, 5);//10个5
    string s("hello");
    vector<char> v3(s.begin(), s.end());
    vector<int> v4{1,2,3,4,5,6,7,8,9,0};   // C++11
    vector<int> v5(v4);

    //打印
    // for+下标
    for (size_t i = 0; i < v1.size(); ++i)
    {
        cout << v1[i] << " ";
    }
    cout << endl;

    // 范围for
    for (auto e : v2)
        cout << e << " ";
    cout << endl;

    // 采用迭代器打印
    vector<char>::iterator it = v3.begin();
    while (it != v3.end())
    {
        cout << *it << " ";
        ++it;
    }
    cout << endl;

    // vector<int>::reverse_iterator rit = v3.rbegin();
    auto rit = v3.rbegin();
    while (rit != v3.rend())
    {
        cout << *rit << " ";
        ++rit;
    }
    cout << endl;

}


//resize使用
/*
void resize(size_t newsize, const T& c = T())
假设：vector在resize之前的元素个数为oldsize，容量为oldcapacity
newsize <= oldsize: 将vector中有效元素个数缩小到newsize
   注意：在有效元素个数减少时，vector的容量不会发生改变

newsize > oldsize: 将vecotr中有效元素个数增大到newsize，多出的位置使用c填充
      newsize > oldcapacity  1. 开辟newsize个元素个空间
	                         2. 拷贝元素
							 3. 释放旧空间
*/
void TestVector2()
{
    int array[] = { 1, 2, 3, 4, 5 };
    vector<int> v(array, array+sizeof(array)/sizeof(array[0]));
    cout << v.size() << endl;
    cout << v.capacity() << endl;

    v.resize(10, 8);

    cout << v.size() << endl;
    cout << v.capacity() << endl;

    v.resize(20);
    cout << v.size() << endl;
    cout << v.capacity() << endl;

    v.resize(16);
    v.resize(10);
    v.resize(6);
    v.resize(4);
    v.resize(2);
    v.clear();
}
int main(){
    TestVector2();
    return 0;
}



/*
void reserve(size_t newcapacity)
假设：在reserve之前vector中旧容量的大小为oldcapacity
newcapacity <= oldcapacity: 直接忽略
newcapacity > oldcapacity: reserve直接将空间扩大到newcapacity
   1. 开辟新空间
   2. 拷贝元素
   3. 释放旧空间
*/



//头部和尾部元素获取
void TestVector4()
{
	vector<int> v{ 1, 2, 3, 4, 5 };
	cout << v[0] << endl;

	v[1] += 10;
	v.operator[](1);
	cout << v.at(1) << endl;

	v.front() = 100;
	v.back() = 500;

	cout << v.front() << endl;
	cout << v.back() << endl;
}


//删除和插入
void TestVector6()
{
	vector<int> v{ 1, 2, 3, 4, 5 };

	v.insert(v.begin() + 2, 100);
	v.insert(v.end(), 10, 6);

	int array[] = { 9, 8, 7, 6, 5 };
	v.insert(v.begin(), array, array + sizeof(array) / sizeof(array[0]));

	v.erase(v.begin());
	v.erase(v.begin(), v.begin() + 5);
	v.erase(v.begin(), v.end());   // v.clear();
}

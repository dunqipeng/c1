/*
vector:1
vector:1  2
vector:1  2  3
vector:1  2  3   4
vector:1  2  3   4   5
*/

void TestVector2(int n)
{
	/*
	vector<vector<int>> vv;
	vv.resize(n);
	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j <= i; ++j)
		{
			vv[i].push_back(j + 1);
			// vv[i][j] = j + 1;   // 下标越界导致程序崩溃
		}
	}
	*/


	vector<vector<int>> vv(n, vector<int>());

	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j <= i; ++j)
		{
			vv[i].push_back(j + 1);
			// vv[i][j] = j + 1;   // 下标越界导致程序崩溃
		}
	}
}


/*
// 矩阵
vector:1  1  1  1   1
vector:2  2  2  2   2
vector:3  3  3  3   3
vector:4  4  4  4   4
vector:5  5  5  5   5
...
*/
void TestVector3(int n)
{
	/*
	vector<vector<int>> vv;
	vv.resize(n);
	for (size_t i = 0; i < n; ++i)
	{
		for (size_t j = 0; j < 5; ++j)
			vv[i].push_back(i + 1);
	}
	*/

	/*
	vector<vector<int>> vv(n, vector<int>());
	for (size_t i = 0; i < n; ++i)
	{
		vv[i].resize(5, i + 1);
	}
	*/

	vector<vector<int>> vv(n, vector<int>(5,0));
}

#include <queue>


int main()
{
	// 默认按照小于方式比较, 创建的是大堆
	priority_queue<int> q;
	q.push(7);
	q.push(1);
	q.push(4);
	q.push(9);
	q.push(2);
	q.push(6);
	q.push(8);
	q.push(3);
	q.push(5);
	q.push(0);

	cout << q.top() << endl;
	cout << q.size() << endl;

	q.pop();
	q.pop();
	q.pop();
	cout << q.top() << endl;
	cout << q.size() << endl;
	return 0;
}



#include <functional>

int test()
{
	// 要创建小堆---必须要按照大于的方式比较
	priority_queue<int,vector<int>, greater<int>> q;
	q.push(7);
	q.push(1);
	q.push(4);
	q.push(9);
	q.push(2);
	q.push(6);
	q.push(8);
	q.push(3);
	q.push(5);
	q.push(0);

	cout << q.top() << endl;
	cout << q.size() << endl;

	q.pop();
	q.pop();
	q.pop();
	cout << q.top() << endl;
	cout << q.size() << endl;
	return 0;
}

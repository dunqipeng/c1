// queue: 是将list重新包装形成一种新的结构---容器适配器
	template<class T, class Container = list<T>>
	class queue
	{
	public:
		queue()
			:_con()
		{}

		void push(const T& data)
		{
			_con.push_back(data);
		}

		void pop()
		{
			_con.pop_front();
		}

		size_t size()const
		{
			return _con.size();
		}

		bool empty()const
		{
			return _con.empty();
		}

		T& front()
		{
			return _con.front();
		}

		const T& front()const
		{
			return _con.front();
		}

		T& back()
		{
			return _con.back();
		}

		const T& back()const
		{
			return _con.back();
		}
	private:
		Container _con;
	};
}
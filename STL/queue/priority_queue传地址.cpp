
//////////////////////////////////第一种函数指针
#include <queue>

class Date
{
public:
	Date(int year, int month, int day)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator<(const Date& d)const
	{
		if ((_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day))
		{
			return true;
		}

		return false;
	}
private:
	int _year;
	int _month;
	int _day;
};

// 函数指针
bool IsLess(Date* left, Date* right)
{
	return *left < *right;
}

typedef bool (*Com)(Date* left, Date* right);

int main()
{
	Date d1(2022, 4, 17);
	Date d2(2022, 4, 18);
	Date d3(2022, 4, 16);

	priority_queue<Date*, vector<Date*>, Com> q1(IsLess);
	q1.push(&d1);
	q1.push(&d2);
	q1.push(&d3);
	return 0;
}


//////////////////////////////////第二种仿函数
// 方式二：仿函数--->也称为函数对象---可以像函数调用一样使用的对象
// 在类中将()重载一下   ():称为函数调用运算符
class Com
{
public:
	bool operator()(Date* left, Date* right)
	{
		return *left < *right;
	}
};

int test()
{
	Date d1(2022, 4, 17);
	Date d2(2022, 4, 18);
	Date d3(2022, 4, 16);

	Com com;
	com.operator()(&d1, &d2);

	// com不是函数，是一个对象
	// 但是com对象可以像函数一样使用----因此com可以将其称为函数对象
	// 函数对象：
	com(&d1, &d2);

	priority_queue<Date*, vector<Date*>, Com> q1;
	q1.push(&d1);
	q1.push(&d2);
	q1.push(&d3);
	return 0;
}
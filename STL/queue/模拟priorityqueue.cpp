#include <iostream>
using namespace std;
#include <vector>
#include <functional>
template<typename T,typename Container=vector<T>,class Com=less<T>>
class priorityqueue{
private:
    Container _con; // 默认就是vector;
    void AdjustDown(std::size_t parent){
        Com com;
        std::size_t child=2*parent+1;
        while(child<this->size()){
            if(child+1<this->size()&&com(_con[child],_con[child+1])){
                child++;
            }
            if(com(_con[parent],_con[child])){
                swap(_con[parent],_con[child]);
                parent=child;
                child=child*2+1;
            }else
                break;;
        }
    }
    void _AdjustUp(std::size_t child){
        std::size_t parent=(child-1)/2;
        Com com;
        while(child){
            if(com(_con[parent],_con[child])){
                swap(_con[parent],_con[child]);
                child=parent;
                parent=(parent-1)/2;
            }else
                break;
        }
    }

public:
    priorityqueue():_con(){
    }

    template<typename Iterator>
    priorityqueue(Iterator first, Iterator last):_con(first,last){
        for(int root=(this->size()-2)/2;root>=0;root--){
            this->AdjustDown(root);
        }
    }
    void push(const T& data){
        _con.push_back(data);
        this->_AdjustUp(this->size()-1);
    }
    void pop(){
        if(this->empty()){
            return;
        }
        swap(_con.front(),_con.back());
        _con.pop_back();
        this->AdjustDown(0);
    }
    bool empty()const{
        return _con.empty();
    }
    std::size_t size()const{
        return _con.size();
    }
   const T& top()const{
        return _con.front();
    }

};
#include "RBTree.hpp"

namespace bite
{
	template<class K>
	class set
	{
		typedef K ValueType;
		

		struct KeyOfValue
		{
			const K& operator()(const ValueType& value)
			{
				return value;
			}
		};

		typedef RBTree<ValueType, KeyOfValue> Tree;

	public:
		typedef typename Tree::iterator iterator;
	public:
		set()
			:_t()
		{}

		/////////////////////////////////////////
		// 
		iterator begin()
		{
			return _t.Begin();
		}

		iterator end()
		{
			return _t.End();
		}

		////////////////////////////////////////////
		// 
		size_t size()const
		{
			return _t.Size();
		}

		bool empty()const
		{
			return _t.Empty();
		}

		///////////////////////////////////////////
		// ޸
		pair<iterator, bool> insert(const ValueType& value)
		{
			return _t.Insert(value);
		}

		void swap(set<ValueType>& s)
		{
			_t.Swap(s._t);
		}

		void clear()
		{
			_t.Clear();
		}

		//////////////////////////////////////
		iterator find(const K& key)
		{
			return _t.Find(key);
		}
	private:
		Tree _t;
	};
}

#include <iostream>
#include <string>
using namespace std;

void TestSet()
{
	bite::set<string> m;
	m.insert("orange");
	m.insert("peach");
	m.insert("apple");
	m.insert("banana");

	cout << m.size() << endl;

	auto it = m.begin();
	while (it != m.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;


	auto ret = m.find("grape");
	if (ret == m.end())
	{
		m.insert("grape");
	}

	for (auto& e : m)
	{
		cout << e <<" "<< endl;
	}

	m.clear();
	if (m.empty())
	{
		cout << "ok" << endl;
	}
	else
	{
		cout << "error" << endl;
	}
}
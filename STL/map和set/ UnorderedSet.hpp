#pragma once
#include "Common.h"
#include "开散列应用、unordermap模拟.cpp"

namespace bite
{
	template<class K, class Hash = T2DDef<K>>
	class unordered_set
	{
		typedef K ValueType;

		struct KeyOfValueType
		{
			const K& operator()(const ValueType& value)const
			{
				return value;
			}
		};

		typedef HashBucket<ValueType, KeyOfValueType, Hash> HB;

	public:
		typedef typename HB::iterator iterator;
	public:
		unordered_set()
			: _hb()
		{}

		////////////////////////////////////////////
		/// 
		iterator begin()
		{
			return _hb.begin();
		}

		iterator end()
		{
			return _hb.end();
		}

		/////////////////////////////////////////
		size_t size()const
		{
			return _hb.size();
		}

		bool empty()const
		{
			return _hb.Empty();
		}


		iterator find(const K& key)
		{
			return _hb.Find(key);
		}

		pair<iterator, bool> insert(const ValueType& value)
		{
			return _hb.InsertUnique(value);
		}

		size_t erase(const K& key)
		{
			return _hb.EraseUnique(key);
		}

		void clear()
		{
			_hb.clear();
		}

		void swap(unordered_set<K, Hash>& m)
		{
			_hb.swap(m._hb);
		}

		size_t bucket_count()const
		{
			return _hb.BucketCount();
		}

		size_t bucket_size(size_t bucketNo)const
		{
			return _hb.BucketSize(bucketNo);
		}

		size_t bucket(const K& key)const
		{
			return _hb.Bucket(key);
		}
	private:
		HB _hb;
	};
}


#include <iostream>
using namespace std;

#include <string>

void TestUnorderedSet()
{
	bite::unordered_set<string, Str2D> s;
	s.insert("apple");
	s.insert("orange");
	s.insert("grape");
	s.insert("watermelen");
	s.insert("peach");
	s.insert("pear");
	s.insert("banana");

	cout << s.size() << endl;
	for (auto e : s)
		cout << e << " ";
	cout << endl;

	s.erase("watermelen");

	cout << "Ͱĸ" << s.bucket_count() << endl;
	for (size_t i = 0; i < s.bucket_count(); ++i)
	{
		cout << i << "Ͱ" << s.bucket_size(i) << endl;
	}
	cout << s.bucket("apple") << endl;

	cout << "================" << endl;

	for (auto e : s)
		cout << e << " ";
	cout << endl;

	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}
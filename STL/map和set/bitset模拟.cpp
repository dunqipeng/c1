
#include <vector>
#include <assert.h>
#include <iostream>
using namespace std;

template<std::size_t N>
class bitset1{
public:
    bitset1()
    :_bast(N/8+1)
    {}
    void set(std::size_t pos){
        assert(pos<N);
        std::size_t whichByte=pos/8;
        std::size_t whichBite=pos%8;
        this->_bast[whichByte]|=1<<whichBite;
        _count++;
    }
    void reset(size_t pos){
        assert(pos<N);
        std::size_t whichByte=pos/8;
        std::size_t whichBite=pos%8;
        this->_bast[whichByte]&=~(1<<whichBite);
        _count--;
    }
    bool test(std::size_t pos){
        assert(pos<N);
        std::size_t whichByte=pos/8;
        std::size_t whichBite=pos%8;
        return this->_bast[whichByte]&=1<<whichBite;
    }
    std::size_t size (){
        return N;
    }
    std::size_t count(){
        //1.
        return _count;
        //2.一个字节每个状态1的个数
     /*   int bitCnttable[256] = {
                0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2,
                3, 3, 4, 3, 4, 4, 5, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3,
                3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3,
                4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4,
                3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5,
                6, 6, 7, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4,
                4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5,
                6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 2, 3, 3, 4, 3, 4, 4, 5,
                3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 3,
                4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6,
                6, 7, 6, 7, 7, 8 };

        size_t count1 = 0;
        for (auto e : _bst)
            count1 += bitCnttable[e];*/
    }
private:
    vector<unsigned char>_bast;
    size_t  _count;
};
int main(){
    int array[] = { 1, 3, 7, 4, 12, 16, 19, 13, 22, 18 };

    // 先要预算总共需要多少个比特位
    bitset1<23> bs;

    // 将数组中的元素映射到位图中
    for (auto e : array)
        bs.set(e);

//    cout << bs.count() << endl;
//    cout << bs.size() << endl;

    if (bs.test(13))
        cout << "13 is in array" << endl;
    else
        cout << "13 is not in array" << endl;

    bs.reset(13);
    if (bs.test(13))
        cout << "13 is in array" << endl;
    else
        cout << "13 is not in array" << endl;
    return 0;
}

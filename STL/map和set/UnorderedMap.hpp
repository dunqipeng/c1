#pragma once
#include "Common.h"
#include "开散列应用、unordermap模拟.cpp"

namespace bite
{
	template<class K, class V, class Hash = T2DDef<K>>
	class unordered_map
	{
		typedef pair<K, V> ValueType;

		struct KeyOfValueType
		{
			const K& operator()(const ValueType& value)const
			{
				return value.first;
			}
		};

		typedef HashBucket<ValueType, KeyOfValueType, Hash> HB;

	public:
		typedef typename HB::iterator iterator;
	public:
		unordered_map()
			: _hb()
		{}

		////////////////////////////////////////////
		/// 
		iterator begin()
		{
			return _hb.begin();
		}

		iterator end()
		{
			return _hb.end();
		}

		/////////////////////////////////////////
		size_t size()const
		{
			return _hb.size();
		}

		bool empty()const
		{
			return _hb.Empty();
		}

		/////////////////////////////////////
		// 元素访问
		V& operator[](const K& key)
		{
			return (*(_hb.InsertUnique(make_pair(key, V())).first)).second;
		}

		iterator find(const K& key)
		{
			return _hb.Find(make_pair(key, V()));
		}

		pair<iterator, bool> insert(const ValueType& value)
		{
			return _hb.InsertUnique(value);
		}

		size_t erase(const K& key)
		{
			return _hb.EraseUnique(make_pair(key, V()));
		}

		void clear()
		{
			_hb.clear();
		}

		void swap(unordered_map<K, V, Hash>& m)
		{
			_hb.swap(m._hb);
		}

		size_t bucket_count()const
		{
			return _hb.BucketCount();
		}

		size_t bucket_size(size_t bucketNo)const
		{
			return _hb.BucketSize(bucketNo);
		}

		size_t bucket(const K& key)const
		{
			return _hb.Bucket(make_pair(key, V()));
		}
	private:
		HB _hb;
	};
}


#include <iostream>
using namespace std;

void TestUnorderedMap()
{
	bite::unordered_map<int, int> m;
	m.insert(make_pair(44, 44));
	m.insert(make_pair(12, 12));
	m.insert(make_pair(78, 78));
	m.insert(make_pair(54, 54));
	m.insert(make_pair(11, 11));
	m.insert(make_pair(34, 34));
	m.insert(make_pair(67, 67));
	m.insert(make_pair(90, 90));
	m.insert(make_pair(55, 55));

	cout << m[67] << endl;
	cout << m.size() << endl;
	for (auto e : m)
		cout << e.first << "--->" << e.second << " "<<endl;
	cout << endl;

	m.erase(34);
	m.erase(44);

	cout << "桶的个数：" << m.bucket_count() << endl;
	for (size_t i = 0; i < m.bucket_count(); ++i)
	{
		cout << i << "桶：" << m.bucket_size(i) << endl;
	}
	cout << m.bucket(55) << endl;

	cout << "================" << endl;

	for (auto e : m)
		cout << e.first<<"--->"<<e.second << " "<<endl;
	cout << endl;

	auto it = m.begin();
	while (it != m.end())
	{
		cout << it->first << "--->" << it->second << " " << endl;
		++it;
	}
	cout << endl;
}
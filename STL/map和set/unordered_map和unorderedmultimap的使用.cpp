void TestUnorderedMap()
{
	unordered_map<string, string> m;
	m.insert(pair<string, string>("orange", "橘子"));
	m.insert(pair<string, string>("apple", "苹果"));
	m.insert(make_pair("banana", "香蕉"));
	m.insert(make_pair("grape", "葡萄"));
	m.insert(make_pair("peach", "桃"));
	m.insert(make_pair("watermelen", "西瓜"));

	for (auto& e : m)
		cout << e.first << "--->" << e.second << endl;
	cout << endl;

	auto it = m.begin();
	while (it != m.end())
	{
		cout << it->first << "--->" << it->second << endl;
		++it;
	}

	cout << m.size() << endl;
	m.insert(make_pair("orange", "橙子"));
	cout << m["orange"] << endl;
	for (auto& e : m)
		cout << e.first << "--->" << e.second << endl;
	cout << endl;

	m["pear"] = "梨";

	m.erase("apple");

	cout << "桶的个数：" << m.bucket_count() << endl;
	for (size_t i = 0; i < m.bucket_count(); ++i)
	{
		cout << i << "号桶:" << m.bucket_size(i) << endl;
	}

	cout << m.bucket("pear") << endl;
	cout << m.bucket("apple") << endl;
}

void TestUnorderedMultiMap()
{
	unordered_multimap<string, string> m;
	m.insert(pair<string, string>("orange", "橘子"));
	m.insert(pair<string, string>("apple", "苹果"));
	m.insert(make_pair("banana", "香蕉"));
	m.insert(make_pair("grape", "葡萄"));
	m.insert(make_pair("peach", "桃"));
	m.insert(make_pair("watermelen", "西瓜"));

	for (auto& e : m)
		cout << e.first << "--->" << e.second << endl;
	cout << endl;

	auto it = m.begin();
	while (it != m.end())
	{
		cout << it->first << "--->" << it->second << endl;
		++it;
	}

	cout << m.size() << endl;
	m.insert(make_pair("orange", "橙子"));
	//cout << m["orange"] << endl;
	for (auto& e : m)
		cout << e.first << "--->" << e.second << endl;
	cout << endl;

	//m["pear"] = "梨";

	m.erase("apple");

	cout << "桶的个数：" << m.bucket_count() << endl;
	for (size_t i = 0; i < m.bucket_count(); ++i)
	{
		cout << i << "号桶:" << m.bucket_size(i) << endl;
	}

	cout << m.bucket("orange") << endl;
	cout << m.bucket("apple") << endl;
}

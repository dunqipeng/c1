#include "RBTree.hpp"


namespace bite
{
	template<class K, class V>
	class map
	{
		typedef pair<K, V> ValueType;

		struct KeyOfValue
		{
			const K& operator()(const ValueType& value)
			{
				return value.first;
			}
		};

		typedef RBTree<ValueType, KeyOfValue> Tree;

	public:
		typedef typename Tree::iterator iterator;
	public:
		map()
			:_t()
		{}

		/////////////////////////////////////////
		// 
		iterator begin()
		{
			return _t.Begin();
		}

		iterator end()
		{
			return _t.End();
		}

		////////////////////////////////////////////
		// 
		size_t size()const
		{
			return _t.Size();
		}

		bool empty()const
		{
			return _t.Empty();
		}

		///////////////////////////////////////
		// Ԫطʲ
		V& operator[](const K& key)
		{
			return (*(_t.Insert(make_pair(key, V())).first)).second;
		}

		///////////////////////////////////////////
		// ޸
		pair<iterator, bool> insert(const ValueType& value)
		{
			return _t.Insert(value);
		}

		void swap(map<K, V>& m)
		{
			_t.Swap(m._t);
		}

		void clear()
		{
			_t.Clear();
		}

		//////////////////////////////////////
		iterator find(const K& key)
		{
			return _t.Find(make_pair(key, V()));
		}
	private:
		Tree _t;
	};
}

#include <iostream>
#include <string>
using namespace std;

void TestMap()
{
	bite::map<string, string> m;
	m.insert(pair<string, string>("orange", ""));
	m.insert(make_pair("peach", ""));
	m.insert(make_pair("apple", "ƻ"));
	m.insert(make_pair("banana", "㽶"));

	cout << m.size() << endl;

	auto it = m.begin();
	while (it != m.end())
	{
		cout << it->first << "--->" << it->second << endl;
		++it;
	}
	cout << endl;

	cout << m["orange"] << endl;
	m["orange"] = "";
	cout << m["orange"] << endl;

	auto ret = m.find("grape");
	if (ret == m.end())
	{
		m["grape"] = "";
	}

	for (auto& e : m)
	{
		cout << e.first << "--->" << e.second << endl;
	}

	m.clear();
	if (m.empty())
	{
		cout << "ok" << endl;
	}
	else
	{
		cout << "error" << endl;
	}
}
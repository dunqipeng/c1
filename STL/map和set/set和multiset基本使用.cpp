#include <set>

int main()
{
	int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	set<int> s(array, array + sizeof(array) / sizeof(array[0]));

	for (auto e : s)
		cout << e << " ";
	cout << endl;

	s.insert(s.begin(), 100);
	for (auto e : s)
		cout << e << " ";
	cout << endl;

	// key不允许被修改，如果要修改，先删除后插入
	// auto it = s.find(100);
	// *it = 10;

	s.erase(100);
	s.insert(10);
	cout << "正向迭代器打印" << endl;
	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	cout << "反向迭代器打印" << endl;
	auto rit = s.rbegin();
	while (rit != s.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
	return 0;
}


#include <set>

// multiset: key  key可以重复的  less+关于key升序
// 特性：排序
int main()
{
	int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	multiset<int> s(array, array + sizeof(array) / sizeof(array[0]));

	for (auto e : s)
		cout << e << " ";
	cout << endl;

	s.insert(s.begin(), 100);
	for (auto e : s)
		cout << e << " ";
	cout << endl;

	// key不允许被修改，如果要修改，先删除后插入
	// auto it = s.find(100);
	// *it = 10;

	s.erase(100);
	s.insert(10);
	cout << "正向迭代器打印" << endl;
	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	cout << "反向迭代器打印" << endl;
	auto rit = s.rbegin();
	while (rit != s.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
	return 0;
}

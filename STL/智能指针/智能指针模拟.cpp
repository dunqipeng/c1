#include <iostream>
using namespace std;
#include <memory>
// C++98中的auto_ptr就是按照如下方式实现的：
template<class T>
class auto_ptr1
{
public:
    // RAII
    auto_ptr1(T* ptr = nullptr)
            : _ptr(ptr)
    {}

    ~auto_ptr1()
    {
        if (_ptr)
        {
            delete _ptr;
            _ptr = nullptr;
        }
    }

    //////////////////////////////////
    // 具有指针类似的行为
    T& operator*()
    {
        return *_ptr;
    }

    T* operator->()
    {
        return _ptr;
    }

    ///////////////////////////////////
    // 解决浅拷贝的方式：资源转移
    auto_ptr1(auto_ptr1<T>& ap)
            : _ptr(ap._ptr)
    {
        ap._ptr = nullptr;
    }

    auto_ptr<T>& operator=(auto_ptr<T>& ap)
    {
        if (this != &ap)
        {
            if (_ptr)
                delete _ptr;

            _ptr = ap._ptr;
            ap._ptr = nullptr;
        }

        return *this;
    }
private:
    T* _ptr;
};
struct A
{
    int a;
    int b;
    int c;
};


void TestAutoPtr()
{
    auto_ptr1<int> ap1(new int);
    *ap1 = 100;

    auto_ptr1<A> ap2(new A);
    ap2->a = 100;

    // auto_ptr的实现：将ap2管理的资源转移给ap3管理
    // ap2不管理任何资源---内部的指针指向nullptr
   auto_ptr1<A> ap3(ap2);

   auto_ptr1<A> ap4;
    ap4 = ap3;
    ap3->b = 100;
    ap3->c = 200;


    int* pa = new int;
    int* pb(pa);
    *pa = 10;
    *pb = 20;
    delete pa;
}
int main(){
    TestAutoPtr();
    return 0;
}
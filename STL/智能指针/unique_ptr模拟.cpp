#include <iostream>

using namespace std;
#include <memory>
namespace dun {
    template<class T>
    class unique_ptr {
    public:
        // RAII
        unique_ptr(T *ptr = nullptr)
                : _ptr(ptr) {}

        ~unique_ptr() {
            if (_ptr) {
                delete _ptr;
                _ptr = nullptr;
            }
        }

        // 具有指针类似的行为
        T &operator*() {
            return *_ptr;
        }

        T *operator->() {
            return _ptr;
        }
// 解决浅拷贝方式：防拷贝--不允许拷贝：不允许调用拷贝构造 以及 赋值运算符重载
        // C++11
        // 在默认成员函数之后跟上=delete：显式告诉编译器该成员函数不用生成了
        unique_ptr(const unique_ptr<T>&) = delete;
        unique_ptr<T>& operator=(const unique_ptr<T>&) = delete;

        // void func() = delete;

    /**    // C++98
    private:
        unique_ptr(const unique_ptr<T>&);
        unique_ptr<T>& operator=(const unique_ptr<T>&);*/
    private:
        T *_ptr;
    };
};
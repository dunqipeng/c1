	template<class T>
	class auto_ptr
	{
	public:
		// RAII
		auto_ptr(T* ptr = nullptr)
			: _ptr(ptr)
			, _isOwner(false)
		{
			if (_ptr)
				_isOwner = true;
		}

		~auto_ptr()
		{
			if (_ptr && _isOwner)
			{
				delete _ptr; 
				_ptr = nullptr;
			}
		}

		//////////////////////////////////
		// 具有指针类似的行为
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		///////////////////////////////////
		// 解决浅拷贝的方式：资源管理权转移
		// 资源管理：指对资源释放权利的转移
		auto_ptr(const auto_ptr<T>& ap)
			: _ptr(ap._ptr)
			, _isOwner(ap._isOwner)
		{
			ap._isOwner = false;
		}

		auto_ptr<T>& operator=(const auto_ptr<T>& ap)
		{
			if (this != &ap)
			{
				if (_ptr && _isOwner)
					delete _ptr;

				_ptr = ap._ptr;
				_isOwner = ap._isOwner;
				ap._isOwner = false;
			}

			return *this;
		}
	private:
		T* _ptr;
		mutable bool _isOwner;  // 资源真正的拥有者
	};
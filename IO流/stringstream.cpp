#include <iostream>
using namespace std;
#include <sstream>
//数值 转化
int test1()
{
    int a = 12345678;
    string sa;
    // 将一个整形变量转化为 字符串，存储到string类对象中
    stringstream s;
    s << a;
    s >> sa;
    // clear()
     // 注意多次转换时，必须使用clear将上次转换状态清空掉
     
    // stringstreams在转换结尾时(即最后一个转换后),会将其内部状态设置为badbit
    // 因此下一次转换是必须调用clear()将状态重置为goodbit才可以转换
    // 但是clear()不会将stringstreams底层字符串清空掉

    // s.str("");
    // 将stringstream底层管理string对象设置成"",
    // 否则多次转换时，会将结果全部累积在底层string对象中

    s.str("");
    s.clear(); // 清空s, 不清空会转化失败
    double d = 12.34;
    s << d;
    s >> sa;
    return 0;
}
//字符串拼接，允许有不同类型的进行拼接
int main()
{
    stringstream sstream;
    // 将多个字符串放入 sstream 中
    sstream << "first" << " " << "string,"<<123;
    sstream << " second string";

    cout << "strResult is: " << sstream.str() << endl;
    // 清空 sstream
    sstream.str("");
    sstream << "third string";
    cout << "After clear, strResult is: " << sstream.str() << endl;
    return 0; }

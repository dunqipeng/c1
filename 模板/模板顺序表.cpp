#include<iostream>
using namespace std;
template<typename T>
class SeqList{
public:
    SeqList(int capacity)
            :_array(new T[capacity]),
             _capacity(capacity),
             _size(0)
    {}
    ~SeqList(){
        if(this->_array){
            delete []_array;
            this->_capacity=0;
            this->_size=0;
            this->_array= nullptr;
        }
    }
    void pushBack(const T& a){
        checkCapacity();
        this->_array[this->_size++]=a;
    }
    void popBack(){
        if(isEmpty()){
            return;
        }
        this->_size--;
    }
    bool isEmpty(){
        return 0==this->_size;
    }
    int Capacity()const {
        return this->_capacity;
    }
    int size()const {
        return this->_size;
    }
    //[]运算符重载
    T& operator[](int index){
        return this->_array[index];
    }
    T& back();
    T& front();
private:
    T* _array;
    int _capacity;
    int _size;
    void checkCapacity(){
        if(this->_size==this->Capacity()){
            T *temp=new T[2*this->_capacity];
            for(int i=0;i<this->_size;i++)
                temp[i]=this->_array[i];
            delete [] this->_array;
            this->_array=temp;
            this->_capacity*=2;
        }
    };
};
template<typename T>
T& SeqList<T>::back(){
    return this->_array[this->_size-1];
};
template<typename T>
T& SeqList<T>::front(){
    return this->_array[0];
}


class Date{
    friend ostream& operator<<(ostream& ccout,const Date& d);
public:
    Date(int year=1900,int month=1,int day=2)
            :_year(year),
             _month(month),
             _day(day){}
private:
    int _year;
    int _month;
    int _day;
};
ostream& operator<<(ostream& ccout,const Date& d){
    ccout<<d._year<<' '<<d._month<<' '<<d._day<<endl;
}
int main(){
    SeqList<int>a1(5);
    a1.pushBack(2);
    cout<<a1.back();
    cout<<a1[0];//运算法重载
    SeqList<Date>a2(10);
    a2.pushBack( Date(2,6,5));
    cout<<a2.back();
    return 0;
}
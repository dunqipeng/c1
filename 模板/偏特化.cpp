#include<iostream>
#include <cstring>
using namespace std;
template<class T1, class T2>
class Data
{
public:
    Data()
    {
        cout << "Data<T1, T2>" << endl;
    }
private:
    T1 _d1;
    T2 _d2;
};
//形式1
// 只要第二个参数是int类型，不管第一个参数是什么类型，都使用部分特化版本
template<typename T1>
class Data<T1,int>
{
public:
    Data()
    {
        cout << "Data<T1, int>" << endl;
    }
private:
    int _d1;
    T1 _d2;
};
//形式2
//只要两个参数是指针类型的，使用该特化版本
template<class T1, class T2>
class Data<T1*,T2*>
{
public:
    Data()
    {
        cout << "Data<T1*, T2*>" << endl;
    }
private:
    T1* _d1;
    T2* _d2;
};
int main()
{
    // 在对Data类模板实例化时，只要第二个参数不是int类型，都使用的是类模板

    Data<int, int > d1;
    Data<double, int> d2;
    Data<char, char> d3;
    Data<int, char> d4;
    Data<int*, char*> d5;
    return 0;
}

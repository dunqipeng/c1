#include<iostream>
#include <cstring>
using namespace std;
template<class T1, class T2>
class Data
{
public:
    Data()
    {
        cout << "Data<T1, T2>" << endl;
    }
private:
    T1 _d1;
    T2 _d2;
};
template<>
class Data<int, char>
{
public:
    Data()
    {
        cout << "Data<int, char>" << endl;
    }
private:
    int _d1;
    char _d2;
};
int main()
{
    //除了d4调用的特化版本，其他调用的都是模板。
    Data<int, int > d1;
    Data<double, double> d2;
    Data<char, char> d3;
    Data<int, char> d4;
    return 0;
}

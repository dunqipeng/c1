#include <iostream>
using namespace std;
class Complex{
public:

    double a;
    double b;
    Complex(double a1=1,double b1=1)
    :a(a1),
    b(b1){};
    Complex operator+(const Complex& b)const{

        return Complex(this->a+b.a,this->b+b.b);
    }
    friend ostream& operator<<(ostream& _cout,const Complex& d);
};
ostream& operator<<(ostream& _cout,Complex& d){
    _cout<<d.a<<' '<<d.b<<endl;
}




// 函数模板的模板参数列表  作用：告诉编译器T是一个类型
//typename也可以替换为class
template <typename T>
T add(T a,T b){
    return a+b;
}

// 注意：函数模板并不是一个真正的函数，只是一个模具
template <class T1,class T2>
void test(T1 b,T2 a){
    cout<<typeid(T1).name()<<' '<<b<<' '<<a<<endl;
}


int main()
{
    cout<<add(3,2)<<endl;
    test(3,'b');
    Complex a(2.3,3.0);
    Complex b(2.1,2);
    Complex c=add(a,b);
    cout<<c;
    return 0;
}

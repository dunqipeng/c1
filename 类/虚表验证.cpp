
class Base
{
public:
	Base(int b = 10)
		: _b(b)
	{
		cout << "Base()" << endl;
	}

	virtual void f1()
	{
		cout << "Base::f1()" << endl;
	}

	virtual void f2()
	{
		cout << "Base::f2()" << endl;
	}

	virtual void f3()
	{
		cout << "Base::f3()" << endl;
	}

	

	int _b;
};

class Derived : public Base
{
public:
	virtual void f4()
	{
		cout << "Derived::f4()" << endl;
	}

	virtual void f1()
	{
		cout << "Derived::f1()" << endl;
	}

	virtual void f3()
	{
		cout << "Derived::f3()" << endl;
	}

	virtual void f5()
	{
		cout << "Derived::f5()" << endl;
	}

	int _d;
};

#include <string>


typedef void (*PVF)();
// 加typedef之后，PVF就是函数指针类型  如果不加typedef，PVF就是函数指针变量

void PrintVFT(Base& b, const string& desc)
{
	cout << desc << endl;
	PVF* pfv = (PVF*)(*(int*)&b);   // &b-->类型Base*
	while (*pfv)
	{
		(*pfv)();
		pfv++;
	}
	cout << endl;
}


void TestVirtual(Base* pb)
{
	pb->f1();
	pb->f2();
	pb->f3();
}

int main()
{
	cout << sizeof(Derived) << endl;
	Base b;
	Derived d;
	TestVirtual(&b);
	TestVirtual(&d);
	PrintVFT(b, "Base VFT:");
	PrintVFT(d, "Derived VFT:");
	return 0;
}
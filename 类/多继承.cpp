class B1
{
public:
	virtual void f1()
	{
		cout << "B1::f1()" << endl;
	}

	virtual void f2()
	{
		cout << "B1::f2()" << endl;
	}

	int _b1;
};

class B2
{
public:
	virtual void f3()
	{
		cout << "B2::f3()" << endl;
	}

	virtual void f4()
	{
		cout << "B2::f4()" << endl;
	}

	int _b2;
};


class D : public B1, public B2
{
public:
	virtual void f1()
	{
		cout << "D::f1()" << endl;
	}

	virtual void f4()
	{
		cout << "D::f4()" << endl;
	}

	virtual void f5()
	{
		cout << "D::f5()" << endl;
	}
	int _d;
};


typedef void(*PVF)();

void PrintVFTB1(B1& b, const string& desc)
{
	cout << desc << endl;
	PVF* pvf = (PVF*)*(int*)&b;
	while (*pvf)
	{
		(*pvf)();
		++pvf;
	}
	cout << "======================="<<endl;
}

void PrintVFTB2(B2& b, const string& desc)
{
	cout << desc << endl;
	PVF* pvf = (PVF*)*(int*)&b;
	while (*pvf)
	{
		(*pvf)();
		++pvf;
	}
	cout << "=======================" << endl;
}


int main()
{
	cout << sizeof(D) << endl;

	D d;
	d._b1 = 1;
	d._b2 = 2;
	d._d = 3;

	PrintVFTB1(d, "B1 of D VFT:");
	PrintVFTB2(d, "B2 of D VFT:");
	return 0;
}
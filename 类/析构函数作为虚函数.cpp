class A
{
public:
	A(int a)
		: _a(a)
	{
		cout << "A()" << endl;
	}

	virtual void f1()
	{
		cout << "A::f1()" << endl;
	}

	virtual ~A()
	{
		cout << "~A()" << endl;
	}
protected:
	int _a;
};

class B : public A
{
public:
	B(int b, int a)
		: A(a)
		, _b(b)
	{
		cout << "B()" << endl;
		_p = new int[10];
	}

	virtual void f1()
	{
		cout << "B::f1()" << endl;
	}

	void f()
	{
		cout << "f()" << endl;
	}

	virtual ~B()
	{
		delete[] _p;
		cout << "~B()" << endl;
	}
protected:
	int _b;
	int* _p;
};


// 对象类型
// 静态类型：申明变量时候所给的类型，编译器编译器代码时候看的是静态类型
// 动态类型：指针或引用实际指向的类型---运行时

void TestVirtual(A* pa)
{
	// 编译时看的是静态类型，编译器会根据pa之前的类型，
	// 到该类中检测是否有对应的成员函数或者成员变量
	// 有，并且访问权限满足，通过编译，否则报错
	pa->f1();
	//pa->f();
}

void Test()
{
	A* pa = new B(1, 2);

	delete pa;
}
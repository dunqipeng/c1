#include <iostream>
using namespace std;

#include <string>



class Animal
{
public:
	Animal(string name, string gender, int age)
		: _name(name)
		, _gender(gender)
		, _age(age)
	{}

	// 动物叫
	virtual void Bark()
	{
		cout << "动物叫" << endl;
	}
protected:
	string _name;
	string _gender;
	int _age;
};


class Dog : public Animal
{
public:
	Dog(string name, string gender, int age)
		: Animal(name, gender, age)
	{}

	virtual void Bark()
	{
		cout << _name << ":旺旺旺~~~" << endl;
	}
};

class Cat : public Animal
{
public:
	Cat(string name, string gender, int age)
		: Animal(name, gender, age)
	{}

	virtual void Bark()
	{
		cout << _name << "喵喵喵~~~" << endl;
	}
};

class Bird : public Animal
{
public:
	Bird(string name, string gender, int age)
		: Animal(name, gender, age)
	{}

	virtual void Bark()
	{
		cout << _name << ":唱歌" << endl;
	}
};

// 本意：不同动物应该按照自己的特性方式发声---多态
// 实际情况：最终打印的都是动物叫
void TestAnimal(Animal& a)
{
	a.Bark();
}

int main()
{
	Dog dog("大黄", "公", 3);
	Cat cat("小橘", "女", 2);
	Bird bird("poly", "母", 1);

	TestAnimal(dog);
	TestAnimal(cat);
	TestAnimal(bird);
	return 0;
}
#include <iostream>
using namespace std;
//  基类 | 父类
class Animal
{
public:
    void Sleep()
    {
        cout << "睡觉" << endl;
    }

    void Eat()
    {
        cout << "吃饭" << endl;
    }

    string _name;
    string _gender;
    int _age;
};


/*
1. 先要定义一个基类，基类中：放置的是将子类公共部分抽取出来的内容

2. 定义派生类，让其继承自基类

   class 类型 : 继承权限 基类名称
   {
       // ...
   };


*/
// Dog继承了Animal
// Dog：派生类 | 子类
class Dog : public Animal
{
public:
    void Bark()
    {
        cout << "狂吠" << endl;
    }


    string _color;
};


class Cat : public Animal
{
public:
    void Mew()
    {
        cout << "喵~~~" << endl;
    }

    string _temper;
};


class ErHa : public Dog
{
public:
    void Hobby()
    {
        cout << "拆家" << endl;
    }
};


int main()
{
    Dog d;
    d.Eat();
    d.Sleep();
    d.Bark();

    Cat c;
    c.Eat();
    c.Sleep();
    c.Mew();

    return 0;
}
#include <iostream>
using namespace std;
// 在C++中， 动态内存空间申请和释放时，一定要匹配：
// malloc/calloc/realloc---->free
// new ---> delete
// new[] ---> delete[] 
// 否则：可能会存在内存泄漏或者存在程序崩溃（对于内置类型，是否完全匹配，并不会造成内存泄漏或者程序崩溃的后果）
int main()
{   //申请单个元素的空间
    int* p=new int;
    //可以对new出来的空间直接初始化
    int* p1=new int(100);
    //申请连续的空间，无初始化随机值
    int* p2=new int[10];
    //申请并初始化
    int* p3=new int[10]{1,2,3,4,5,6,7,8,9,1};
    //释放
    delete p1;
    delete []p3;
    return 0;
}

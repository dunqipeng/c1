#include <iostream>
#include <typeinfo>
#include <iomanip>
using namespace std;
class Test
{
public:
    Test(int t = 0)
            : _t(t)
    {
        cout << "Test(int):" << this << endl;
    }

 /*   void* operator new(size_t size)
    {
    	return malloc(size);
    }*/


    ~Test()
    {
        cout << "~Test():" << this << endl;
    }
private:
    int _t;
};
void* operator new(size_t size,char* fileName,char* funcName,size_t lineNo){
    cout<<fileName<<"-"<<funcName<<"-"<<lineNo<<":"<<size<<endl;
    return operator new(size);
}
void operator delete(void* p, char* fileName, char* funcName, size_t lineNo)
{
    cout << fileName << "-" << funcName << "-" << lineNo << endl;
    return operator delete(p);
}
#ifdef _DEBUG
#define new new(__FILE__, __FUNCDNAME__, __LINE__)
#define delete(p) operator delete(p, __FILE__, __FUNCTION__, __LINE__)
#endif
int main()
{
    int *pt1=new int;
    delete pt1;
    Test* pt2 = new Test;
    delete pt2;
    return 0;
}

#include <iostream>
using namespace std;
class Test
{
public:
    Test(int t = 0)
            : _t(t)
    {
        cout << "Test(int):" << this << endl;
    }
    ~Test()
    {
        cout << "~Test():" << this << endl;
    }
private:
    int _t;
};

int main()
{
    Test *p=(Test*)malloc(sizeof(Test));
    //new (p) Test;
    new (p)Test(100);

    //现在pt指向的空间已 经是一个Test类型的对象了， 那么在free之前一定要调用析构函数
    // 否则可能 就会存在资源泄漏
    //编译器调用,只有 该情况下调用析构函数
    p->~Test();
    free(p);
    return 0;
}
